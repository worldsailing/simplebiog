# worldsailing SimpleBiog 

### PHP library description
Read only Biog bundle to World Sailing microservices based on Silex.

  
### Install by composer
````php
{
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://worldsailing@bitbucket.org/worldsailing/simplebiog.git"
    }
  ],
  "require": {
    "worldsailing/simplebiog": "dev-master"
  }
}
````

### Usage
````php
use worldsailing\SimpleBiog\provider\SimpleBiogProvider;

$app->register(new SimpleBiogProvider($app), []);

//Then $app['service.simpleBiog'] returns a SimpleBiogService instance
//For example:
$model = $app['service.simpleBiog']->createBiogModel($app);
````

 
### License

All right reserved
