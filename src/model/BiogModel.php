<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\SimpleBiog\model;

use Doctrine\Common\Collections\Criteria;
use worldsailing\Isaf\model\MemberBiogs;

/**
 * Class BiogModel
 * @package worldsailing\SimpleBiog\model
 */
class BiogModel extends AbstractModel
{

    /**
     * BiogModel constructor.
     * @param \Silex\Application $app
     */
    public function __construct($app)
    {
        parent::__construct($app);

    }

    /**
     * @param $data
     * @return bool
     */
    public function validate($data)
    {
        $constraints = MemberBiogs::getConstraints();

        $this->validationErrors = $this->app['validator']->validate($data, $constraints);
        if (count($this->validationErrors) >0) {
            foreach ($this->validationErrors as $error)
                $this->app['monolog']->addDebug($error);
        }
        return count($this->validationErrors) == 0;
    }

    /**
     * @param \Doctrine\Common\Collections\Criteria $criteria
     * @return \Doctrine\Common\Collections\Collection
     */
    public function findAllByCriteria( Criteria $criteria)
    {

        return $this->app['orm.ems']['biog']->getRepository('worldsailing\Isaf\model\MemberBiogs')->matching($criteria);
    }


    /**
     * @param Criteria $criteria
     * @return int
     */
    public function countAllByCriteria( Criteria $criteria)
    {
        return count($this->app['orm.ems']['biog']->getRepository('worldsailing\Isaf\model\MemberBiogs')->matching($criteria));
    }


    /**
     * @param $id
     * @return MemberBiogs
     */
    public function findById($id)
    {
        return $this->app['orm.ems']['biog']->find('worldsailing\Isaf\model\MemberBiogs', $id);
    }

    /**
     * @param $data
     * @return bool
     */
    public function setEntityData( $data )
    {
        $entity = $this->app['orm.ems']['biog']->find('worldsailing\Isaf\model\MemberBiogs', $data['BiogMembId']);

        $entity->setData($data);
        if ($data['BiogMembId'] != 0) {
            $entity->setBiogUpdated(date('Y-m-d H:i:s'));
        }

        try {
            $this->app['orm.ems']['biog']->merge($entity);
            $this->app['orm.ems']['biog']->flush();
        } catch (\Exception $e) {
            $this->app['monolog']->addError($e->getMessage());
            return false;
        }
        return true;
    }
}
