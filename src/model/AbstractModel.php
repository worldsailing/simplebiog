<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\SimpleBiog\model;

use Silex\Application;

/**
 * Class AbstractModel
 * @package worldsailing\SimpleBiog\model
 */
Abstract class AbstractModel {

    /**
     * @var Application
     */
    protected $app;

    /**
     * @var \Symfony\Component\Validator\ConstraintViolationListInterface[]
     */
    protected $validationErrors = [];

    /**
     * AbstractModel constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface[]
     */
    public function getValidationErrors()
    {
        return $this->validationErrors;
    }

    /**
     * @return string
     */
    public function getValidationMessages()
    {
        $message = '';
        foreach ($this->validationErrors as $error) {
            $message .= $error->getPropertyPath().' '.$error->getMessage()."\n";
        }
        return $message;
    }
}
