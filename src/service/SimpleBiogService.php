<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\SimpleBiog\service;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\Criteria;
use worldsailing\Helper\UrlHelper;
use worldsailing\SimpleBiog\model\BiogModel;
use worldsailing\Common\BundleResultSet\CompositeListResultSet;
use worldsailing\Common\BundleResultSet\CompositeEntityResultSet;
use worldsailing\Common\Exception\WsServiceException;
use worldsailing\Api\response\ValidationErrorList;

/**
 * Class SimpleBiogService
 * @package worldsailing\SimpleBiog\service
 */
Class SimpleBiogService {

    /**
     * @var \Silex\Application
     */
    protected $app;

    /**
     * SimpleBiogService constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param Application $app
     * @return BiogModel
     */
    public function createBiogModel(Application $app)
    {
        return new BiogModel($app);
    }

    /**
     * @param BiogModel $model
     * @param Request|null $request
     * @return CompositeListResultSet
     */
    public function getBiogListByRequest(BiogModel $model, Request $request = null)
    {
        if ($request === null) {
            $request = Request::createFromGlobals();
        }

        $limit = UrlHelper::getLimitOfRequest($request);

        $offset = UrlHelper::getOffsetOfRequest($request);

        $order = UrlHelper::getOrderOfRequest($request);

        $criteria = UrlHelper::getCriteriaOfRequest($request);

        return $this->getSimpleBiogListByCriteria($model, $criteria, $order, $limit, $offset);

    }


    /**
     * @param BiogModel $model
     * @param Request|null $request
     * @return int
     */
    public function countBiogListByRequest(BiogModel $model, Request $request = null)
    {
        if ($request === null) {
            $request = Request::createFromGlobals();
        }

        $criteria = UrlHelper::getCriteriaOfRequest($request);

        return $model->countAllByCriteria($criteria);
    }


    /**
     * @param BiogModel $model
     * @param Criteria $criteria
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return CompositeListResultSet
     */
    public function getSimpleBiogListByCriteria(BiogModel $model, Criteria $criteria, $order = [], $limit =0, $offset = 0)
    {
        $count = $model->countAllByCriteria($criteria);

        if (count($order) > 0) {
            $criteria->orderBy($order);
        }

        if ($limit > 0) {
            $criteria->setMaxResults($limit)
                ->setFirstResult($offset);
        }

        $data = $model->findAllByCriteria($criteria)->toArray();

        return (new CompositeListResultSet($data))->setCountAll($count);

    }

    /**
     * @param BiogModel $model
     * @param $id
     * @return CompositeEntityResultSet
     */
    public function getBiogById(BiogModel $model, $id)
    {
        return (new CompositeEntityResultSet($model->findById($id)));
    }


    /**
     * @param BiogModel $model
     * @param $sailorId
     * @return CompositeListResultSet
     */
    public function getBiogBySailorId(BiogModel $model, $sailorId)
    {
        $criteria = new Criteria();
        $criteria->where($criteria::expr()->eq('BiogIsafId', $sailorId));
        $criteria->andWhere($criteria::expr()->eq('BiogDeleted', 'no'));
        $data =  $model->findAllByCriteria($criteria)->toArray();
        return (new CompositeListResultSet($data));

    }

    /**
     * @param BiogModel $model
     * @param $email
     * @return CompositeListResultSet
     */
    public function getBiogByEmail(BiogModel $model, $email)
    {
        $criteria = new Criteria();
        $criteria->where($criteria::expr()->eq('BiogEmail', $email));
        $criteria->andWhere($criteria::expr()->eq('BiogDeleted', 'no'));
        $data = $model->findAllByCriteria($criteria)->toArray();
        return (new CompositeListResultSet($data));
    }

    /**
     * @param BiogModel $model
     * @param Request $request
     * @param $id
     * @return CompositeEntityResultSet
     * @throws WsServiceException|\Exception
     */
    public function setBiogData(BiogModel $model, Request $request, $id) {

        $this->app['monolog']->addDebug($request->__toString());

        if ($this->validateBiogData($model, $request)) { //validation
            /**
             * TODO: Not implemented properly yet. Check BiogModel.php
             */
            if ($model->setEntityData($request->query->all())) {
                return $this->getBiogById($model, $id);
            } else {
                throw new \Exception('An Error Occurred', 500);
            }
        }
    }

    /**
     * @param BiogModel $model
     * @param Request $request
     * @return bool
     * @throws WsServiceException
     */
    public function validateBiogData(BiogModel $model, Request $request)
    {
        if ($model->validate($request->query->all())) {
            return true;
        } else {
            $e = new WsServiceException('Validation error', 400);
            /**
             * \Symfony\Component\Validator\ConstraintViolationListInterface
             */
            $errors = $model->getValidationErrors();
            /**
             * This line creates a standard API Validation Error List format
             *
             * You can read that by $e->__map() as array or $e->__json() as string
             */
            $e->__setData((new ValidationErrorList('errors', $errors)));
            throw $e;
        }
    }
}

