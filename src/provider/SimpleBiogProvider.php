<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\SimpleBiog\provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

use worldsailing\Isaf\provider\IsafOrmProvider;
use worldsailing\SimpleBiog\service\SimpleBiogService;

/**
 * Class SimpleBiogProvider
 * @package worldsailing\SimpleBiog\provider
 */
class SimpleBiogProvider implements ServiceProviderInterface
{

    /**
     * @param Container $app
     */
    public function register(Container $app)
    {

        /*
         * Register Doctrine ORM for affected tables
         */
        if (! isset($app['orm.ems'])) {
            $app->register(new IsafOrmProvider());
        }

        $app['service.simpleBiog'] = function ($app) {
            return new SimpleBiogService($app);
        };

        return;
    }
}
